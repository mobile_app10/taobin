import 'dart:io';

class taobinMachine {
  int price = 0;
  int money = 0;

  showWelcome() {
    print("Welcome to Taobin");
    print("Please input youre dink type.");
  }

  showType() {
    print("1.Coffee");
    print("2.Tea");
    print("3.Milk");
    print("4.Soda");
  }

  showMenuCoffee() {
    print("----------------------- Menu for coffee-----------------------");
    List menu = ['Capuchino', 'Americano', 'Latte', 'Mocka', 'Esspeso'];
    print('Menu: $menu\n');
  }

  showMenuTea() {
    print("----------------------- Menu for Tea-----------------------");
    List menu = [
      'Green tea',
      'Thai tea',
      'Fuji tea',
      'Jasmine tea',
      'Rose tea'
    ];
    print('Menu: $menu\n');
  }

  showMenuMilk() {
    print("----------------------- Menu for Milk-----------------------");
    List menu = [
      'Cocho Milk',
      'Milk',
      'Pink Milk',
      'Green Milk',
      'Chocolate milk'
    ];
    print('Menu: $menu\n');
  }

  showMenuSoda() {
    print("----------------------- Menu for Soda-----------------------");
    List menu = [
      'Red lemon soda',
      'Red soda',
      'Apple soda',
      'Lemon soda',
      'Bluelemon soda'
    ];
    print('Menu: $menu\n');
  }

  int checkTypeSelection(int selection) {
    switch (selection) {
      case 1:
        showMenuCoffee();
        print("Please select your coffee menu:");
        break;
      case 2:
        showMenuTea();
        print("Please select your tea menu:");
        break;
      case 3:
        showMenuMilk();
        print("Please select your milk menu:");
        break;
      case 4:
        showMenuSoda();
        print("Please select your soda menu:");
        break;
      default:
    }
    return selection;
  }
}

class typeDrink {
  showSelectType() {
    print("1.HOT 2.ICE 3.FREBPAE");
    print("Please select your type drink : ");
  }

  showSelectSweetLevel() {
    print("1.Low\n2.Medium\n3.Hight");
    print("Please select your type drink : ");
  }

  int checkTypeSelection(int selection) {
    switch (selection) {
      case 1:
        break;
      case 2:
        break;
      case 3:
        break;

      default:
    }
    return selection;
  }
}

class coffee extends taobinMachine {}

class tea {}

class milk {}

class soda {}

void main() {
  taobinMachine taobin = new taobinMachine();
  typeDrink dt = new typeDrink();
  taobin.showWelcome();
  taobin.showType();
  int select = int.parse(stdin.readLineSync()!);
  taobin.checkTypeSelection(select);
  dt.showSelectSweetLevel();
}
